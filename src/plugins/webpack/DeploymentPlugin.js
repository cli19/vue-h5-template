const { basename } = require('path')
const colors = require('colors')
const compressing = require('compressing')
const Client = require('ssh2').Client

// 宝塔 站点配置文件 /www/server/panel/vhost/nginx
// 配置
// {
//   host: '',
//   port: '22',
//   username: 'root',
//   password: '',
// 	 remoteDir: '/www/wwwroot/remote-test',
//   onlyCompress: false
// }

function conn(config) {
  const connect = new Client()
  connect
    .on('ready', function () {
      // console.log('Client :: ready')
      console.log(colors.green('======== 服务器链接成功 ========'))
      upload(connect, config.savePath, config.remoteDir)
    })
    .connect(config)
}

// 压缩
function start(config) {
  compressing.zip.compressDir(config.outputDir, config.savePath).then(() => {
    console.log(colors.green('======== 压缩成功 ========'))
    if (config.onlyCompress) return
    conn(config)
  })
}

// 服务端解压
function unPackage(connect, remotePath) {
  connect.shell((err, stream) => {
    if (err) {
      console.log(err)
      console.log(colors.red('======== 部署失败 ========'))
      return
    }
    // 服务端解压后保存目录
    const unzipSavePath = remotePath.replace(basename(remotePath), '')
    let buf = ''
    stream
      .on('close', err => {
        connect.end()
      })
      .on('data', data => {
        buf += data
        console.log(buf)
        if (/(centos ~]#)$/.test(buf.trim())) {
          stream.close()
          console.log(colors.green('======== 部署完成 ========'))
        }
      })
      .write(`unzip -o ${remotePath} -d ${unzipSavePath} && echo '--ok--'\n`, 'utf-8')
  })
}

// 上传
function upload(connect, localPath, remoteDir) {
  connect.sftp(function (err, sftp) {
    if (err) throw err
    const remotePath = remoteDir + '/' + basename(localPath)
    sftp.fastPut(localPath, remotePath, (err) => {
      if (err) {
        console.log('err', err)
        connect.end()
      }
      unPackage(connect, remotePath)
    })
  })
}

// savePath 打包后的zip路径
// remoteDir 服务器项目路径 末尾不带反斜杠
class DeploymentPlugin {
  constructor(config = { savePath: '', remoteDir: '' }) {
    this.config = config
  }

  apply(compiler) {
    // 打包后的目录
    const outputDir = compiler.options.output.path
    const customoOutputDir = this.config.outputDir
    const customoSavePath = this.config.savePath
    this.config.outputDir = customoOutputDir || outputDir
    this.config.savePath = customoSavePath || `${outputDir}.zip`
  
    compiler.hooks.afterEmit.tap('WebpackVersionPlugin', () => {
      start(this.config)
    })
  }
}

module.exports = DeploymentPlugin