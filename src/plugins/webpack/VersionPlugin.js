const fs = require('fs')
const path = require('path')
const colors = require('colors')

// 版本文件说明
const defOptions = {
  upgrade: '', // 主版本好发生改变时
  addition: '', // 增加功能时
  fix: '', // 修复
  version: ''
}
class VersionFile {
  constructor(logFilePath, options = defOptions) {
    this.logFilePath = logFilePath
    this.options = options
  }

  apply(compiler) {
    const outputDir = compiler.options.output.path
    // 版本日志
    const outputFile = path.join(outputDir, './version.log')
    compiler.hooks.afterEmit.tap('WebpackVersionPlugin', () => {
      const logFile = this.logFilePath || outputFile || path.resolve(__dirname, './version.log')
      if (fs.existsSync(logFile)) {
        fs.appendFileSync(logFile, this.mark())
      } else {
        fs.writeFileSync(logFile, this.mark())
      }
      // 将日志文件放进打包文件中
      fs.copyFileSync(logFile, outputFile)
      console.log(colors.green('\n版本日志写入完成!!!\n'))
    })
  }

  mark() {
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDay()
    const hour = date.getHours()
    const minutes = date.getMinutes()
    const seconds = date.getSeconds()
    const timestamp = `[${year}/${month}/${day} ${hour}:${minutes}:${seconds}]\n`
    const version = `- Version ${this.options.version}\n`
    const upgrade = this.options.upgrade ? `- UPGRADE\n${this.options.upgrade}\n` : ''
    const addition = this.options.addition ? `- ADDITION\n${this.options.addition}\n` : ''
    const fix = this.options.fix ? `- FIX\n${this.options.fix}\n` : ''
    const content = upgrade + addition + fix
    return timestamp + version + (content || 'No update, only build') + '\n\n'
  }
}

module.exports = VersionFile
